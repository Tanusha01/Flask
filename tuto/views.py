from flask import url_for, redirect
from.app import db
from .models import Albums
from flask.ext.wtf import Form
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired
from .app import app
from flask import render_template
from .models import get_sample, get_album
from wtforms import PasswordField
from .models import User
from hashlib import sha256
from flask.ext.login import login_user, current_user
from flask import request
from flask.ext.login import logout_user
from flask.ext.login import login_required



@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))


class LoginForm(Form):
	username = StringField('Username')
	password = PasswordField('Password')

	def get_authenticated_user(self):
		user = User.query.get(self.username.data)
		if user is None:
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.password else None



class AlbumsForm(Form):
	id = HiddenField('id')
	name = StringField('Nom', validators=[DataRequired()])


@app.route("/")
def home():
	return render_template(
    "home.html",
    title="Music Groupes",
    albums = get_sample())


@app.route("/edit/albums/<int:id>")
@login_required
def edit_author(id):
	a = get_albums(id)
	f = AlbumsForm(id=a.id, name=a.name)
	return render_template(
		"edit-albums.html",
		albums=a, form=f)

@app.route("/edit/album/<int:id>")
def edit_albums(id):
	a = get_album(id)
	f = AlbumsForm(id=a.id, name=a.title)
	return render_template(
		"edit-albums.html",
		albums=a, form=f)

@app.route("/save/albums/", methods=("POST",))
def save_albums():
	a = None
	f = AlbumsForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_album(id)
		a.name = f.name.data
		db.session.commit()
		return redirect(url_for('view_album', id=a.id))
	a = get_album(int(f.id.data))#aucun echeck
	return render_template(
		"edit-albums.html",
		albums=a, form=f)

@app.route("/edit/groupes/<int:id>")
@login_required
def edit_groupes(id):
	a = get_groupe(id)
	f = GroupeForm(id=a.id, name=a.name)
	return render_template(
		"edit-groupes.html",
		albums=a, form=f)

@app.route("/save/groupes/", methods=("POST",))
def save_groupes():
	a = None
	f = GroupeForm()
	if f.validate_on_submit():
		id = int(f.id.data)
		a = get_groupe(id)
		a.name = f.name.data
		db.session.commit()
		return redirect(url_for('one_groupe', id=a.id))
	a = get_groupe(int(f.id.data))#aucun echeck
	return render_template(
		"edit-groupes.html",
		albums=a, form=f)

@app.route("/album/<int:id>", methods=("GET",))#je veux ce  que la route recupere toutes les id d'albumes
#l'adress qui va afficher dans le barre
def view_album(id):
	a = get_album(id)
	return render_template(
		"album.html",
		album=a)#Je veux afficher un album



@app.route("/login/", methods=("GET", "POST",))
def login():
	f = LoginForm()
	if f.validate_on_submit():
		user = f.get_authenticated_user()
		if user:
			login_user(user)
			return redirect(url_for("home"))
	return render_template(
		"login.html",
		form=f)
