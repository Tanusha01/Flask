from .app import db
from flask.ext.login import UserMixin
from .app import login_manager

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)


class Groupe(db.Model):
    id      	=db.Column(db.Integer, primary_key=True)
    name    	=db.Column(db.String(100))

class Albums(db.Model):
    id      	=db.Column(db.Integer, primary_key=True)
    title       =db.Column(db.String(250))
    img         =db.Column(db.String(90))
    releaseYear =db.Column(db.Integer)
    groupe_id	=db.Column(db.Integer, db.ForeignKey("groupe.id"))
    groupe  	=db.relationship("Groupe",
            backref=db.backref("albums", lazy="dynamic"))
    genre_id	=db.Column(db.Integer, db.ForeignKey("genre.id"))
    genre  	=db.relationship("Genre",
            backref=db.backref("albums", lazy="dynamic"))

class Genre(db.Model):
    id      	=db.Column(db.Integer, primary_key=True)
    name        =db.Column(db.String(100))

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))
    def get_id(self):
        return self.username




def get_sample():
	return Albums.query.limit(10).all()
    # get_sample(debut=0,nb=10):
    #Book.query.offset(debut).limit(nb).all()


def get_album(id):
    return Albums.query.get(id)


def get_groupe(id):
    return Groupe.query.get(id)

def get_genre(id):
    return Genre.query.get(id)


#ou return Author.query.filter(Author.id==id).all()[0]
